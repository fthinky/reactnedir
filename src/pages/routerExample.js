import React, { Component } from 'react'
import Divider from '@material-ui/core/Divider';
import H from '../components/h'
import P from '../components/p'
import ListItemLink from '../components/listItemLink'
import LinkIcon from '@material-ui/icons/Link';
import Pre from '../components/pre'
export default class routereExample extends Component {
  render() {
    return (
      <div>
        <br />
        <H>React Router Nedir</H>
        <P>React uygularımızda ihtiyaç duyduğumuz routing(yönlendirme) işlemini javaScript'lerle clientside (istemci) tarafında yapmamızı sağlayan javascript kütüphanesidir.
          npm'den <code>npm install react-router-dom</code> komutu ile indirebilirsiniz. Aşağıda router işlemlerini örneklerle 4.3.1 versiyonunu kullanarak anlatmaya çalışağım. Bu versiyonu <code> npm install react-router-dom@^4</code> komutu ile indirebilirsiniz.
        </P>
        <P>React Router için yararlanabileceğiniz linkler
          <ul>
            <li><a href="https://reacttraining.com/react-router/web/guides/quick-start" target="_blank">reacttraining.com</a></li>
            <li><a href="https://omergulcicek.github.io/react-router/" target="_blank">omergulcicek.github.io</a></li>
            <li><a href="https://www.youtube.com/watch?v=vyPImqapMPE&index=12&list=PLbspYejTXMLAeZTSFh1_wAKeYPhN87ro-" target="_blank">Emrah Öz-Youtube</a></li>
          </ul>
        </P>
        <P>Aşağıdaki örnek üç ana sayfadan oluşuyor.
          <ul>
            <li>Sayfa1</li>
            <P>En Basit Haliyle router yapısının gösteren örnektir. Ayrıca Sayfa2'ye erişme işlemini yapan bir buton ve bir link bulunur.</P>
            <li>Sayfa2</li>
            <P>Bu örnekte, Sayfa2'nin içinde açılan 3 tane alt sayfa vardır. Ayrıca Sayfa1'e erişmek için bir buton vardır.</P>
            <li>Sayfa3</li>
            <P>Bu sayfanın bir tane alt sayfası var, seçilen ürüne göre, seçtiğinin ürünün bilgilerini getiren dinamik bir yapı vardır.
              Mesala, Bir e-ticaret sitesinde, ürünlerinizi gösterecek bir sayfanız olsun. seçtiğiz ürüne göre, o ürün bilgeri bu sayfanızda görünücektir.
              Bu örnek ile bunu sağlayabilirsiniz. Ayrıca query string ile gönderebileceğiniz parametleri nasıl tanımlanız gerektiğini Ürün2 sayfasında görebilirsiniz.
            </P>
          </ul>
        </P>
        <br />
        <P>
        <ListItemLink href="https://codesandbox.io/s/nwn101xn4" target="_blank" text="Kodların çalışır haline buradan bakalirsiniz.">
            <LinkIcon />
          </ListItemLink>
        </P>
        <br/>
        <div>
          <iframe name="codesandbox" src={`https://codesandbox.io/embed/nwn101xn4?codemirror=1&amp;fontsize=14&amp;module=/example.js`} style={{ width: '100%', height: '100vh', outline: '0px', border: '0px', borderRadius: '4px' }}></iframe>
        </div>
        <br />
        <P><code>App.js</code>'de index.html ile iletişimimizi, en dış kompenent <code>HashRouter</code> olacak şekilde oluşturuyoruz. Bu şekilde tüm sayfalarımızı router yapısını kullanabiliyoruz.</P>
        <P><code>HashRouter</code> yerine <code>BrowserRouter</code>'da kullanabilirsiniz. Tercih size kalmış.</P>
        <P>daha detaylı anlatım için <a href="https://omergulcicek.github.io/react-router/gelismis-kilavuzlar/hash-router" target="_blank">HashRouter</a> ve <a href="https://omergulcicek.github.io/react-router/gelismis-kilavuzlar/browser-router" target="_blank">BrowserRouter</a> tıklayınız.</P>
        <Pre>
          {`import { HashRouter } from "react-router-dom"

ReactDOM.render(<HashRouter><div><App /></div></HashRouter>, document.getElementById("root"));`}
        </Pre>
        <br />
        <Divider />
        <br />
        <P>sayfa içi link oluşturma işlemleri</P>
        <Pre>
          {`import { Link } from "react-router-dom";

<Link to="/sayfa1">Sayfa1</Link>`}
        </Pre>
        <P>Butona tıklayarak, sayfa içi yönlendirme</P>
        <Pre>
          {`<button onClick={this.navigateToPage1}>Sayfa1</button>

navigateToPage1 = () => {
  this.props.history.push('/sayfa1');
}`}
        </Pre>

        <P>Seçtiğiniz Linkteki sayfaya yöndermek için</P>
        <Pre>
          {`import { Switch, Route } from "react-router-dom";

<Switch>
  <Route exact path="/" component={Sayfa1} />
  <Route exact path="/sayfa1" component={Sayfa1} />
  <Route path="/sayfa2" component={Sayfa2} />
  <Route path="/sayfa3" component={Sayfa3} />
  <Route component={NoMatch} />
</Switch>
      `}
        </Pre>
        <br />
        <Divider />
        <br />
        <P>sayfa içi query parametre ile link oluşturma işlemleri</P>
        <Pre>
          {`import { Link } from "react-router-dom";

<Link to={{ pathname: "/sayfa3/2", search: "?musteriNo=1234&token=12345" }} > Ürün2 </Link>`}
        </Pre>
        <P>Seçtiğiniz query parametleri linkten sayfaya yönlendirmek için</P>
        <Pre>{`<Switch>
  <Route path="/sayfa3/:number" component={Alt4} />
</Switch>`}</Pre>
<P>Yönlendirilmiş sayfadan query parametlerini okumak.</P>
        <Pre>{`Seçtiğiniz Ürün : {this.props.match.params.number}</p>
Query Parameters:{this.props.location.search}`}</Pre>
      </div>
    )
  }
}
