import React, { Component } from 'react'
import LinkIcon from '@material-ui/icons/Link';
import Divider from '@material-ui/core/Divider';
import H from '../components/h'
import P from '../components/p'
import ListItemLink from '../components/listItemLink'
import Pre from '../components/pre'

export default class cssExample extends Component {
    render() {
        return (
            <div>
                <br />
                <H>CSS Nasıl Yapılır?</H>
                <P>Bir tarayıcı bir stil sayfası okuduğunda, HTML belgesini stil sayfasındaki bilgilere göre biçimlendirir.</P>
                <br/>
                <H>React'ta CSS Eklemenin iki Yolu</H>
                <P>
                    <ul>
                        <li>Dış stil sayfası (External style sheet)</li>
                        <li>Satır içi stil (Inline style)</li>
                    </ul>
                </P>
                <br />
                <Divider />
                <br />
                <H>Dış stil sayfası (External style sheet)</H>
                <P>Harici bir stil sayfasıyla, sadece bir dosyayı değiştirerek tüm web sitesinin görünümünü değiştirebilirsiniz!</P>
                <P>İlk önce oluşturduğunuz css dosyasını react sayfanıza import etmelisiniz. <code>import "../style/main.css"</code> Html tag'larınıza <code>className</code> olarak css'lerinizi ekleyebiliriz.</P>
                <Pre>
                    {
                        `import React, { Component } from 'react'
import "../style/main.css"
export default class CssDemo extends Component {
    render() {
      return (
        <div>
          <div className="my-text">
            Bu kısım "Dış stil sayfası (External style sheet)" ile oluşturuldu.
        </div>
      )
    }
  }`}
                </Pre>
                <br />
                <P>main.css</P>
                <Pre>
                    {`.my-text{
        color:red;
        background-color: blue;
    }`}
                </Pre>
                <br />
                <Divider />
                <br />
                <H>Satır içi stil (Inline style)</H>
                <P>Tek bir öğeye benzersiz bir stil uygulamak için satır içi stil kullanılabilir. Satır içi stilleri kullanmak için stil niteliğini ilgili öğeye ekleyin. Stil özelliği, herhangi bir CSS özelliğini içerebilir.</P>
                <P>React sayfalarında css'ler  Camel Case (Medial Capitals) olarak yazılır. Mesala "text-align" => "textAlign" şeklinde yazılır.</P>

                <Pre>
                    {`const styles = ({
  myText: {
    margin: '40px',
    border: '5px solid pink'
  },
  myTextOther: {
    margin: '40px',
    border: '5px solid blue'
  }
});`}
                </Pre>

                <Pre>
                    {`<div style={styles.myText}>
  Bu kısım Inline styling ile oluşturuldu.
</div>
<div style={styles.myTextOther}>
  Bu kısım Inline styling ile oluşturuldu.
</div>
`}
                </Pre>

                <P>Inline Style aşağıdaki gibi de yapılabilir</P>
                <Pre>{`<div style={{ color: "red", backgroundColor: "blue" }}>
   Bu kısım Inline styling ile oluşturuldu.
</div>`}</Pre>
            </div>
        )
    }
}
