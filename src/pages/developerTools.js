import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import H from '../components/h'
import P from '../components/p'
import ImageCard from '../components/imageCard'
import ListItemLink from '../components/listItemLink'
import LinkIcon from '@material-ui/icons/Link';


export default class DeveloperTools extends Component {

    handleClickButton = (e) => {
        e.stopPropagation();
        window.open("https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi");
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <br />
                <H>
                    React Developer Tools Nedir?
                    </H>
                <br />
                <P>
                    Facebook ekibi tarafından geliştirilen bir tarayıcı eklentisidir. React ile yazılmış sayfalarda, sayfanın kodunu incelemek için açtığınızda,  resimde gördüğünüzü gibi React tabı oluşacaktır. Bu tab sayesinde sayfanızdaki DOM node'larınızı görebilir. State ve Property'lerinizi yönetebilirsiniz.
                <br />
                    <ListItemLink href="https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi" target="_blank" text="Developer Tools İndirme Sayfası">
                        <LinkIcon />
                    </ListItemLink>
                </P>
                <br />
                <ImageCard title="React Developer Tools" imgLink="reactDeveloperTool.jpg" cardContent="Chrome React Developer Tools">
                    <Button size="small" color="primary" onClick={this.handleClickButton}>
                        Developer Tools İndirme Sayfası
                     </Button>
                </ImageCard>

                <br />
                <P>
                    Eklentiyi kurduktan sonra  <code>chrome://extensions/</code> sayfasından <code>Dosya URL'lerine erişime izin ver</code> aktif yapmalısınız.
                    </P>
                <br />
                <ImageCard title="Chrome Ayarları" imgLink="reactDeveloperToolAyarlar.png" />
            </div>
        )
    }
}
