import React, { Component } from 'react'
import LinkIcon from '@material-ui/icons/Link';
import Divider from '@material-ui/core/Divider';
import H from '../components/h'
import P from '../components/p'
import ListItemLink from '../components/listItemLink'
import Pre from '../components/pre'

export default class PropsExample extends Component {
  render() {
    return (
      <div>
        <br />
        <H>Property Nedir? </H>
        <P>Property kavramını anlatabilmek için aşağıki gibi bir örnek üzerinden gidelim.</P>
        <P>
          Bir form sitesinde, bir konu hakkında içerik paylaşımı yapılır. Diğer kullanıcılar bu içerik hakkında sayfanın altında yorumlarını girerler. Bizde React Property kavramını anlatmak için böyle bir demo yazacağız. <br />
          <ListItemLink href="https://codesandbox.io/s/pyk550nw3m" target="_blank" text="Kodların çalışır haline buradan bakalirsiniz.">
            <LinkIcon />
          </ListItemLink>
        </P>


        <H>
          postComments.js
        </H>
        <Pre>
          {`import React from "react";

export default function Post(props) {
  return (
    <div className="Post">
      <h1 className="PostTitle">{props.postTitle}</h1>
      <div className="PostBody">{props.postBody}</div>
      <hr />
      {props.comments.map((comment, index) => {
        const userNameSurname =`}  &#96;&#36;{`{comment.user.name} - `}&#36;{`{comment.user.surname}`}&#96;{`;
        return (
          <div>
            <p className="CommendBody">{index + 1}. yorum:{comment.body}</p>
            <p className="CommendUserInfo">{userNameSurname}</p>
          </div>
        );
      })}
    </div>
  );
}

`}</Pre>
        <br />
        <H>postConst.js</H>
        <Pre>{`export const post = {
  id: 1,
  title:
    "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  body:
    "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"
};
export const comments = [
  {
    id: 1,
    postId: 1,
    body:
      "laudantium enim quasi est quidem magnam voluptate ipsam eos tempora quo necessitatibus dolor quam autem quasi reiciendis et nam sapiente accusantium",
    user: {
      name: "Leanne Graham",
      surname: "Graham"
    }
  },
  {
    id: 2,
    postId: 1,
    body:
      "est natus enim nihil est dolore omnis voluptatem numquam et omnis occaecati quod ullam at voluptatem error expedita pariatur nihil sint nostrum voluptatem reiciendis et",
    user: {
      name: "Ervin Howell",
      surname: "Howell"
    }
  }
];

`}</Pre>
        <br />
        <H>Index.js</H>
        <Pre>{`import React from "react";
import ReactDOM from "react-dom";
import * as PostConts from "./postConst";
import PostComments from "./postComments";
import "./styles.css";

function App() {
  return (
    <div className="App">
      <PostComments
        postTitle={PostConts.post.title}
        postBody={PostConts.post.body}
        comments={PostConts.comments}
      />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);

`}</Pre>

        <br />
        <Divider />
        <br />
        <H>Bileşenleri daha küçük bileşenlere bölmekten korkmayın.</H>
        <P>
          <code>postComments.js</code>'de bir form sitesini canlandırmıştık. burada bir konu hakkında içerik paylaşımı var ve form kullanıcıları bu içerik hakkında sayfanın altında yorumlarını girermişlerdi.
          Yapılan yorumları ayrı bir bileşende kodlayarak sayfamınız daha küçük parçalar halinde oluşturabiliriz.
          <ListItemLink href="https://codesandbox.io/s/7v20rmv6j" target="_blank" text="Kodların çalışır haline buradan bakalirsiniz."><LinkIcon /></ListItemLink>
        </P>
        <Pre>
          {`import React, { Component } from "react";

function Post(props) {
  return (
    <div className="Post">
      <h1 className="PostTitle">{props.postTitle}</h1>
      <div className="PostBody">{props.postBody}</div>
      <hr />
      {Comment(props)}
    </div>
  );
}
export default Post;

Comment = props => {
  return (
    <div>
      {props.comments.map((comment, index) => {
        const userNameSurname =`}  &#96;&#36;{`{comment.user.name} - `}&#36;{`{comment.user.surname}`}&#96;{`;
        return (
          <div>
            <p className="CommendBody">
              {index + 1}. yorum:{comment.body}
            </p>
            <p className="CommendUserInfo">{userNameSurname}</p>
          </div>
        );
      })}
    </div>
  );
};


`}</Pre>

        {/* propery function
    prop-types
*/}
      </div>
    )
  }
}

