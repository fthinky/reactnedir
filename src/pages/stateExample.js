import React, { Component } from 'react'
import LinkIcon from '@material-ui/icons/Link';
import Divider from '@material-ui/core/Divider';
import H from '../components/h'
import P from '../components/p'
import ListItemLink from '../components/listItemLink'
import Pre from '../components/pre'

export default class StateExample extends Component {
  render() {
    return (
      <div>
        <br />
        <H>State Nedir?</H>
        <P>State, component içinde kullanılan ve değiştirilme durumu olan verileri tutan yapıdır. Örnek olarak, React, sayfa üzerindeki Html DOM'u(Document Object Model) direkt olarak erişmediği için, inputlardaki değerleri state üzerinde tutan bir sayfa tasarlayalım.</P>
        <P>Aşağıdaki örnekte, text alanına girdiğimiz değeri state üzerinde tutan bir yapı tasarlayacağız. "Bilgi Getir" butonuna tıkladığımızda yazdığımız değeri uyarı olarak ekranda göstereceğiz. </P>
        <ListItemLink href="https://codesandbox.io/s/3rv8zn99vq" target="_blank" text="Kodların çalışır haline buradan bakalirsiniz.">
            <LinkIcon />
          </ListItemLink>
        <P>Sınıf bileşenimizin kurucu metodunda state tanımını yapalım.</P>
        <Pre>
          {`this.state = {
            name: ""
          };`}
        </Pre>
        <P>Html Text'te value değerini, state'de tanımladığımız "name" alanında tutuyoruz. Text alanına bir değer girdiğimizde, bu değer "onChange" metodunda çağırdığımız fonksiyon ile güncelleyeceğiz.</P>
        <Pre>
          {`<input type="text" value={this.state.name} onChange={this.handleChangeName} />`}
        </Pre>
        <P>"handleChangeName" metodundaki "setState" ile girilen değeri state.name'e yazıyoruz.</P>
        <Pre>{`handleChangeName = event => {
    this.setState({
      name: event.target.value.toUpperCase()
    });
  };`}</Pre>

  <P>"Bilgi Getir" butounu</P>
  <Pre>{`<button onClick={this.handleSubmit}>Bilgi Getir</button>`}</Pre>
<P>handleSubmit metodu</P>
<Pre>{`handleSubmit = event => {
    event.preventDefault();
    event.stopPropagation();
    alert(this.state.name);
  };`}</Pre>
  <br/>
  <Divider/>
  <br/>
  <H>State içerisinde Dizi tanımla</H>
  <P>İçerisinde renk item'ları olan bir combobox'ımız olsun. Bu combobox'ta birden fazla seçim yapabilme hakkımız olsun. Bu örneği state içerisindeki bir dizi'de tutalım.</P>
  <ListItemLink href="https://codesandbox.io/s/p94nv3mo2x" target="_blank" text="Kodların çalışır haline buradan bakalirsiniz.">
            <LinkIcon />
          </ListItemLink>
  <P>State</P>
  <Pre>{`this.state = {
      colors: []
    }`}</Pre>
    <br/>
    <P>Combobox</P>
  <Pre>{
    `<label>
    Renk Seçimi:
    <select value={this.state.colors} onChange={this.handleChangeColor} multiple={true}>
      <option value=""></option>
      <option value="mavi">Mavi</option>
      <option value="yesil">Yeşil</option>
      <option selected value="kirmizi">Kırmızı</option>
      <option value="sari">Sarı</option>
    </select>
  </label>`
}</Pre>
<br/>
<P>handleChangeColor metodu</P>
<Pre>{`handleChangeColor = (event) => {
    var index = this.state.colors.indexOf(event.target.value)
    if (index !== -1) {
      this.setState({
        colors: this.state.colors.filter(clr => {
          return clr !== event.target.value
        })
      });
    }
    else {
      this.setState({
        colors: [...this.state.colors, event.target.value]
      });
    }
  }`}</Pre>
  <br/>
  <Divider/>
  <br/>
  <H>State içerisinde Object tanımla</H>
  <P>State içerisinde aşağıdaki örnekte olduğu gibi object ve object array'de tanımlayabilirsiniz.</P>
  <Pre>{`editedUser: {
                name: "",
                email: ""
            },
            userList: [
                {
                    name: "fatih",
                    email: "fatih@gmail.com"
                },
                {
                    name: "mehmet",
                    email: "mehmet@gmail.com"
                }
            ]
`}</Pre>
      </div>
    )
  }
}
