import React, { Component } from 'react'
import ListItemLink from '../components/listItemLink';
import ListITemFolderLink from '../components/listITemFolderLink';

// import List from '@material-ui/core/List';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemText from '@material-ui/core/ListItemText';
// import DashboardIcon from '@material-ui/icons/Dashboard';
// import LinkIcon from '@material-ui/icons/Link';
// import FolderIcon from '@material-ui/icons/Folder';
// import StarBorder from '@material-ui/icons/StarBorder';
// import ExpandLess from '@material-ui/icons/ExpandLess';
// import ExpandMore from '@material-ui/icons/ExpandMore';
// import Collapse from '@material-ui/core/Collapse';
export default class Menu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: true,
          };
    }
    // handleClick = () => {
    //     this.setState(state => ({ open: !state.open }));
    //   };
    
    render() {
        return (
            <div>
                <ListItemLink href="#/" text="Ana Sayfa" />
                <ListItemLink href="#/giris" text="Giriş" />
                <ListItemLink href="#/ilk-proje" text="React projesi oluşturma" />
                <ListItemLink href="#/react-developer-tools" text="React Developer Tools" />
                <ListItemLink href="#/components" text="Component" />
                <ListItemLink href="#/props" text="Property" />
                <ListItemLink href="#/propsChildren" text="Props.children" />
                <ListITemFolderLink text="State">
                <ListItemLink isFromFolder={true} href="#/states" text="State" style={{marginLeft:"16px"}} />
                <ListItemLink isFromFolder={true} href="#/setState" text="SetState" style={{marginLeft:"16px"}} />
                </ListITemFolderLink>
                <ListItemLink href="#/css" text="Css oluşturma" />
                <ListItemLink href="#/router" text="Router" />
                {/* <ListItem button onClick={this.handleClick}>
                    <ListItemIcon>
                        <FolderIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Css" />
                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button style={{marginLeft:"16px"}}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText inset primary="Starred" />
                        </ListItem>
                        <ListItem button style={{marginLeft:"16px"}}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText inset primary="Starred2" />
                        </ListItem>
                    </List>
                </Collapse> */}
            </div>
        )
    }
}
