import React, { Component } from 'react'
import H from '../components/h'
import P from '../components/p'
import Pre from '../components/pre'
import ListItemLink from '../components/listItemLink'
import LinkIcon from '@material-ui/icons/Link';
export default class ComponentsExample extends Component {
    render() {
        return (
            <div>
                <br />
                <H>
                    Komponent Nedir?
                </H>
                <P>Komponentler, UI birimlerini birbirinden bağımsız ve yeniden kullanılabilir parçalara ayırabilmemizi sağlar.
                Aşağıda Class, Function ve Const olarak aynı işi yapan 3 komponent yazacağız. <br />
                    <ListItemLink href="https://codesandbox.io/s/rm3mlx41r4" target="_blank" text="Kodların çalışır haline buradan bakalirsiniz.">
                        <LinkIcon />
                    </ListItemLink>
                </P>


                <H>
                    Class
                </H>
                <Pre>
                    {`import React from "react";
class WelcomeClass extends React.Component {
  render() {
    return <div>Merhaba {this.props.name}</div>;
  }
}

export default WelcomeClass;
`}
                </Pre>
                <br />
                <H>
                    Function
                </H>

                <Pre>
                    {`import React, { Component } from "react";
function WelcomeFunc(props) {
  return <div>Merhaba {props.name}</div>;
}
export default WelcomeFunc;
`}
                </Pre>
                <br />
                <H>
                    Const
                </H>
                <Pre>
                    {`import React, { Component } from "react";

const WelcomeConst = props => <div>Merhaba {props.name}</div>;

export default WelcomeConst;
`}
                </Pre>
                <br />
                <H>
                    Index.js
                </H>
                <Pre>
                    {`import React from "react";
import ReactDOM from "react-dom";
import WelcomeClass from "./welcomeClass";
import WelcomeConst from "./welcomeConst";
import WelcomeFunc from "./welcomeFunc";
import "./styles.css";

function App() {
  return (
    <div className="App">
      <WelcomeClass name="Ali" />
      <WelcomeConst name="Veli" />
      <WelcomeFunc name="Ahmet" />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
`}
                </Pre>

                <P>Sonuç olarak html çıktımız asağıdaki gibi olacaktır.</P>
                <P>Merhaba Ali</P>
                <P>Merhaba Veli</P>
                <P>Merhaba Ahmet</P>
            </div>
        )
    }
}
