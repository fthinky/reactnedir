import React, { Component } from 'react'
import Divider from '@material-ui/core/Divider';
import H from '../components/h'
import P from '../components/p'
import ListItemLink from '../components/listItemLink'
import LinkIcon from '@material-ui/icons/Link';
import Pre from '../components/pre'
export default class SetStateExample extends Component {
    render() {
        return (
            <div>
                <br />
                <H>SetState Kavramını Anlama</H>
                <P>State ve setState kavramının daha iyi anlaşılması için iki önemli konudan bahsedeceğim.</P>
                <br />
                <Divider />
                <br />
                <H>1. Konu</H>
                <P>Set State kavramını aşağıdaki örnek üzerinden anlatalım. Bu örnekte, bir sayaç yapalım. Artır butonuna tıklandığında 1 artan, azalt butonuna tıklandığında bir azalan.</P>

                <div>
                    <iframe src="https://codesandbox.io/embed/j7mz2wv3l3" style={{ width: '100%', height: '200px', border: '0', borderRadius: '4px', overflow: 'hidden' }} sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
                </div>

                <P>Bu örnekte sayacı artırmak için <code>{`this.setState({sayac: this.state.sayac + 1});`} </code> kod satırı kullanıldı. Artır butonuna tıklandığında bu kod satırını aşağıdaki gibi 3 defa da yazsaksa 1 defa artığını göreceğiz. </P>
                <Pre>
                    {` handleClickAzalt = () => {
    this.setState({ sayac: this.state.sayac - 1 });
    this.setState({ sayac: this.state.sayac - 1 });
    this.setState({ sayac: this.state.sayac - 1 });
  };
  handleClickArtir = () => {
    this.setState({ sayac: this.state.sayac + 1 });
    this.setState({ sayac: this.state.sayac + 1 });
    this.setState({ sayac: this.state.sayac + 1 });
  };`}
                </Pre>
                <P>Bunun nedeni <code><a href="https://developer.mozilla.org/tr/docs/Web/JavaScript/Reference/Global_Objects/Object/assign">Object.assign()</a></code> gibi davranmasından kaynaklıdır.</P>
                <Pre>{`Object.assign(  
  {},
  { sayac: this.state.sayac + 1 },
  { sayac: this.state.sayac + 1 },
  { sayac: this.state.sayac + 1 },
)`}</Pre>

                <P>Bu durumu düzeltmek için setState fonksiyon çağrısı olacak şekilde yapmamız gerekiyor.</P>

                <div>
                    <iframe src="https://codesandbox.io/embed/y3j2qlv7x9" style={{ width: '100%', height: '200px', border: '0', borderRadius: '4px', overflow: 'hidden' }} sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
                </div>
                <Pre>
                    {` this.setState((state, props) => ({ sayac: state.sayac + 1 }));`}
                </Pre>

                <H>Kaynakça:</H>
                <P>
                    <a href="https://reactjs.org/docs/state-and-lifecycle.html#state-updates-may-be-asynchronous" target="_blank">reactjs.org#setstate</a></P>
                <P>
                    <a href="https://css-tricks.com/understanding-react-setstate/" target="_blank">understanding-react-setstate</a>
                </P>


                <br />
                <Divider />
                <br />
                <H>2. Konu</H>
                <P>SetState ile state'imizi günceledikten sonra aynı yaşam döngüsü içerisinde güncellediğimiz stati kullanmak istersek dikkat etmemiz gereken durumlar vardır. Bunu gene sayaç örneğimiz üzerinden anlatalım.
                    Sayaç örneğimizde artır veya azalt butonlarına tıkladıktan sonra <code>alert()</code> ile ekranda sayacımızın güncel halini ekranda göstermeye çalışalım.
                </P>
                <div>
                    <iframe src="https://codesandbox.io/embed/xl7nwmrwqq" style={{ width: '100%', height: '200px', border: '0', borderRadius: '4px', overflow: 'hidden' }} sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
                </div>
                <P>Örnekte gördünüz gibi render içerisinde sayaçın son hali doğru gösterilirken, <code>alert()</code> ile verdiğimiz popup mesajda state'in güncellenmeden önceki halini görüyoruz.</P>
                <P>Bu şekilde kullanım bize hatalı sonuç verecektir:</P>
                <Pre>{` this.setState((state, props) => ({ sayac: state.sayac + 1 }));
 alert(this.state.sayac);`}</Pre>
                <P> Doğru kullanımı iki şekilde sağlayabiliriz.
</P>
                <H>1. yol olarak:</H>
                <P>SetState parantezleri içerisinde <code>alert()</code> işlemini gerçekleştirmek.</P>

                <div>
                    <iframe src="https://codesandbox.io/embed/33599o336" style={{ width: '100%', height: '200px', border: '0', borderRadius: '4px', overflow: 'hidden' }} sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
                </div>
                <Pre>{`this.setState(
      (state, props) => ({
        sayac: state.sayac + 1
      }),
      () => alert(this.state.sayac)
    );`}</Pre>
                <br />
                <H>2. Yol: Async / Await Keywordleri ile</H>
                <P>JavaScript asenkron çalışın bir dildir. C# ve Java'daki gibi senkron değildir. Yani fonk1() ve fonk2() olarak iki fonksiyonu peşpeşe çağırdığınız zaman önce fonk1()'in tamamnanmasını bekleyip sonra fonk2() nin çalışmasını beklerseniz hata yapmış olursunuz. JavaScript'de senkran çalışmak istiyorsanız  Async / Await kullanabilirsiniz. Daha detaylı bilgi için <a href="https://www.gencayyildiz.com/blog/ecmascript-6-async-await-keywordleri/" target="_blank">async-await-keywordleri</a></P>
                <P>Gene sayaç örneğimizi  Async / Await Keywordleri ile oluşturalım.</P>
                <div>
                <iframe src="https://codesandbox.io/embed/z6wvkvkp3p" style={{ width: '100%', height: '200px', border: '0', borderRadius: '4px', overflow: 'hidden' }} sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
                </div>
                <Pre>
                    {`  handleClickArtir = async () => {
    await this.setState({ sayac: this.state.sayac + 1 });
    alert(this.state.sayac);
  };`}
                </Pre>
            </div>
        )
    }
}
