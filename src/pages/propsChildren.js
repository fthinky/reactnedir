import React, { Component } from 'react'
import H from '../components/h'
import P from '../components/p'
import ListItemLink from '../components/listItemLink'
import LinkIcon from '@material-ui/icons/Link';
import Pre from '../components/pre'

export default class propsChildren extends Component {
  render() {
    return (
      <div>
        <br />
        <H>Props.children Nedir? </H>
        <P>
          Bir komponenet (bileşen) oluşturduğunuzda, o bileşen tag'ları arasında yazdığınız node'ları görmenizi sağlar. İsterseniz bir örnek ile açıklayalım.
         <ListItemLink href="https://codesandbox.io/s/j4k484ymv5" target="_blank" text="Kodların çalışır haline buradan bakalirsiniz.">
            <LinkIcon />
          </ListItemLink>
        </P>
        <Pre>
          {`function App() {
  return (
    <div className="App">
      <Welcome name="Ahmet">
        <span>hoşgeldin</span>
      </Welcome>
    </div>
  );
}
`}
        </Pre>
        <P>
          Welcome bileşen tagları arasına bir tane span oluşturduk. Bu span'ı ekranda görmek için Welcome bileşenimizin içine <code>this.props.children</code> yazmamız gerekiyor.
                </P>

        <Pre>
          {`export default class Welcome extends Component {
  render() {
    return (
      <div>
        Bu sayfaya {this.props.children} {this.props.name}
      </div>
    );
  }
}
`}
        </Pre>

      </div>
    )
  }
}
