import React, { Component } from 'react'
import H from '../components/h'
import P from '../components/p'
import Pre from '../components/pre'
export default class Main extends Component {
    render() {
        return (
            <div>

                <br />

                <H>React nedir?</H>
                <P>
                    React, Facebook tarafından geliştiren bir javascript kütüphanesidir.</P>
                <P>
                    Facebook'taki geliştiriciler kod bakımı ile ilgili bazı sorunlarla karşılaşmaya başladı. Facebook Reklamları uygulaması artan sayıda özelliğe sahip olduğundan, bunu yönetmek için 2011’de Jordan Walke, Ract’in ilk prototipi olan FaxJS’i oluşturdu.
                    Bundan bir yıl sonra, yani 2012 yılında Jordan Walke, React’i oluşturdu.</P>
                <P>
                    Instagram 9 Nisan tarihinde Facebook tarafından satın alındı. Instagram ekibi Facebook’un bu yeni teknolojisini benimsemek istedi. Böylece React açık kaynaklı hale getirildi.
                </P>
                <P>
                    Böylece react dünya üzerinde popülaritesi sürekli artarak gelişimine devam etmektedir.
                </P>


                <Pre>
                    {``}<span className="token keyword">{`class`}</span>
                    <span className="token class-name"> HelloMessage</span>
                    <span className="token keyword"> extends </span>
                    <span className="token class-name">
                        React<span className="token punctuation">.</span>Component  </span>
                    <span className="token punctuation">&#123;  </span>
                    {`
 `}
                    <span className="token function">render</span><span className="token punctuation">(</span><span className="token punctuation">)</span> <span className="token punctuation">&#123;</span>
                    {`
  `}
                    <span className="token keyword">return</span> <span className="token punctuation">(</span>
                    {`
   `}
                    <span className="token tag"><span className="token tag"><span className="token punctuation">&lt;</span>div</span><span className="token punctuation">&gt;</span></span>
                    {`
     `}
                    Hello <span className="token punctuation">&#123;</span><span className="token keyword">this</span><span className="token punctuation">.</span>props<span className="token punctuation">.</span>name<span className="token punctuation">}</span>
                    {`
   `}
                    <span className="token tag"><span className="token tag"><span className="token punctuation">&lt;/</span>div</span><span className="token punctuation">&gt;</span></span>
                    {`
  `}
                    <span className="token punctuation">)</span><span className="token punctuation">;</span>
                    {`
 `}
                    <span className="token punctuation">}</span>
                    <br />
                    <span className="token punctuation">}</span>
                    <br />
                    <br />
                    ReactDOM<span className="token punctuation">.</span><span className="token function">render</span><span className="token punctuation">(</span>
                    {`
 `}
                    <span className="token tag"><span className="token tag"><span className="token punctuation">&lt;</span>HelloMessage</span> <span className="token attr-name">name</span><span className="token attr-value"><span className="token punctuation">=</span><span className="token punctuation">"</span>World<span className="token punctuation">"</span></span> <span className="token punctuation">/&gt;</span></span><span className="token punctuation">,</span>
                    {`
 `}
                    document<span className="token punctuation">.</span><span className="token function">getElementById</span><span className="token punctuation">(</span><span className="token string">'hello-example'</span><span className="token punctuation">)</span>
                    <br />
                    <span className="token punctuation">)</span><span className="token punctuation">;</span>
                </Pre>

                <br />


            </div>
        )
    }
}
