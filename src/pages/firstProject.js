import React, { Component } from 'react'
import H from '../components/h'
import P from '../components/p'
import ImageCard from '../components/imageCard'
import Pre from '../components/pre'
import Divider from '@material-ui/core/Divider'

export default class FirstProject extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div>
                <br />

                <P>Bu sayfada React projemizi Visual Studio Code editörü kullanarak, Webpack ve Babel ile birlikte sıfırdan oluşturacağız.</P>
                <P>Haydi! İlk uygulmama geliştirmeye başlayalım.</P>
                <br />
                <P>Çalışma yapmak istediğiniz klasörü oluşturduktan sonra Visual Studio Code üzerinden File --> Open Folder yaparak klasörünüzü, editörde açabilirsiniz.
                 Terminal gözükmüyorsa View --> Terminal seçimini yaparak Terminal’i açabilirsiniz. Terminali npm komutlarımızı yazmak için kullanacağız. Node.js kurulumununu henüz yapmadıysanız <a href="https://nodejs.org/en/">Node.js</a>  adresinden “Recommended For Most Users” versiyonunu kurabilirsiniz.
               </P>
                <P>
                    Terminalde <code>npm init</code> yazarak ilk olarak <code>package.json</code> dosyasını oluşturuyoruz. <code>npm init</code> yazdıktan sonra bizden <code>package-name, version, description</code> gibi alanları doldurabiliceğimiz satırlar geliyor. Bu satırları doldurabilir veya şimdilik enter’a basarak ilerleyebilirsiniz. Daha sonra dosyayı açıp içerisinde değişikler yapabilirsiniz.
                    Yada <code>npm init -y</code> komutu ile <code>package.json</code> dosyasını direkt oluşturabilirsiniz.
               </P>
                <H>Package.json nedir?</H>
                <ImageCard title="package.json" imgLink="npmInit2.png" cardContent="package.json, Projeniz hakkında bilgiler içeren bir dosyadır." />
                <br />
                <H>Webpack nedir?</H>
                <ImageCard title="webpack" imgLink="webpack.png" cardContent="Webpack, npm paketlerini kullanarak yazdığımız projemizi bize paketleyerek tek bir .js çıktısı veren bir araçtır. Tarayıcılar bizim yazdığımız react scriptlerini anlamazlar. React ile yazdığımız scriptler webpack ile derlenerek, tarayıcılar için anlamlı javascript kütüphanesi oluşturulur." />
                <br />
                <H>Babel nedir?</H>
                <ImageCard title="babel" imgLink="babel.png" cardContent="JavaScript ECMAScript standartlarında oluşturulmuş bir programlama dilidir. Teknoloji geliştikçe yıllar içerisinde ECMAScript yeni versiyonları çıkar. Tarayıcılar bu versiyonlara hemen adaptasyon sağlayamayabiliyor. ES5 üstü ile yazdığımız javascript kodlarını, tüm browserların anlayacağı ES5 versiyonuna dönüştürmemizi sağlayacak yapıdır." />
                <br />
                <P>
                    Hadi şimdi bunları projemize yükleyelim
                </P>
                <Pre>

                    {`npm i react --save
npm i react-dom –save
npm i prop-types --save
npm i webpack --save-dev
npm i webpack-cli --save-dev
npm i babel-core babel-loader babel-preset-env babel-preset-react –save-dev
npm i html-webpack-plugin html-loader --save-dev
npm i css-loader style-loader --save-dev
npm i webpack-dev-server --save-dev
npm i babel-plugin-transform-class-properties --save
`}
                </Pre>
                <P>
                    <ul>
                        <li>
                            <strong>babel-core :</strong> ES6 kodunuzu ES5'e dönüştürür
                        </li>
                        <li>
                            <strong>babel-loader :</strong> JavaScript bağımlılıklarınızı dönüştürmek için (örneğin, bileşenlerinizi diğer bileşenlere içe aktarırken) Babel ile Webpack yardımcısı
                        </li>
                        <li>
                            <strong>babel-preset-env :</strong> Desteklemek istediğiniz tarayıcı matrisine dayanarak hangi dönüşümlerin / eklentilerin kullanılacağını ve çoklu doldurmaların (doğal olarak desteklemeyen eski tarayıcılarda modern işlevsellik sağladığını) belirler
                        </li>
                        <li>
                            <strong>babel-preset- react:</strong> Tüm React eklentileri için Babil önceden ayarlanmış, fonksiyonlara dönüştürme
                        </li>
                        <li>
                            <strong>webpack-dev-server:</strong> tarayıcıdaki değişikliklerinizi her görmek istediğinizde bu komutu çalıştırmaya devam etmek biraz sıkıcıdır. Web paketinin değişikliklerimizi “izlemesi” ve böylece herhangi bir bileşenimizde değişiklik yaptığımız zaman yenilemesi için bu modülünü kullanabiliriz.
                        </li>
                        <li>
                            <strong>babel-plugin-transform-class-properties:</strong> Bu eklenti, es2015 statik sınıf özelliklerini ve es2016 özellik başlatıcı sözdizimi ile bildirilen özellikleri dönüştürür.
                        </li>
                    </ul>
                </P>
                <br />
                <Divider />
                <br />
                <H>Package.json</H>
                <ImageCard title="ilkProje" imgLink="ilkProje.png" />
                <br />
                <P> Proje dosya dizinlerimizi resimdeki sol menüdeki gibi oluşturalım. <br />
                    <code>npm install</code> işlemlerinden sonra, package.json dosyasında dependencies ve devDependencies son hali resimdeki gibi oluştuğunu göreceksiniz.
                     scripts kısmına aşağıdaki start ve build komutlarını ekleyelim.
                     build ile webpack projemizi derleyecek, start ile de localhost'ta projemizi çalıştıracağız.
                                <Pre>{` "scripts": {
    "start": "webpack-dev-server --open --mode development",
    "build": "webpack --mode production"
  },`}</Pre>

                </P>
                <br />
                <Divider />
                <br />
                <H>.babelrc ayarları</H>
                <ImageCard title="babelrc" imgLink="babelrc.png" />
                <br />
                <P>.babelrc isminde babel configirasyon dosyası oluşturalım.
                                <Pre>{`{
  "presets": ["env", "react"],
  "plugins":["transform-class-properties"]
}`}</Pre>
                </P>
                <br />
                <Divider />
                <br />
                <H>webpack.config.js ayarları</H>
                <ImageCard title="webpackConfig" imgLink="webpackConfig.png" />
                <br />
                <P>
                    webpack.config.js isminde webpack configirasyon dosyası oluşturalım.
                <Pre>{`const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins


module.exports = {
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            }

        ]
    },
    mode: 'production',
    plugins: [
        new HtmlWebpackPlugin({ template: './index.html', filename: "./index.html" })
    ]
};
`}</Pre>

                </P>

                <br />
                <Divider />
                <br />
                <H>index.html</H>
                <ImageCard title="indexHtml" imgLink="indexHtml.png" />
                <br />
                <P><Pre>{`<!DOCTYPE html>
<html lang="tr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="./style/main.css">
    <title>Başlık</title>  
</head>
<body>
    <div id="root" class="root">
    <script src="./dist/bundle.js"></script>
</body>
</html>`}</Pre>
                </P>

                <br />
                <Divider />
                <br />
                <H>app.js</H>
                <ImageCard title="appjs" imgLink="appjs.png" />
                <br />
                <P><Pre>{`import React, { Component } from 'react'
import ReactDOM from 'react-dom';

export default class App extends Component {
  render() {
    return (
      <div>
       <p className="colorRed">Merhaba Dünya</p> 
      </div>
    )
  }
}

   ReactDOM.render(<App />, document.getElementById('root')
   );`}</Pre>
                </P>

                <br />
                <Divider />
                <br />
                <H>main.css</H>
                <ImageCard title="style" imgLink="mainCss.png" />
                <P>
                    <Pre>{`.colorRed{
    color:red;
}`}</Pre>

                </P>

                <P>
                    Artık webpack ile projeyi derleyelim. package.json'da yazdığımız build script komutunu kullalım.
              Terminal'de <code>npm run-script build</code> komutunu çalıştıralım.
                </P>
                <br />
                <Divider />
                <br />
                <H>Bir Hata, Bir Not!!!</H>
                <ImageCard title="BabelLoaderError" imgLink="BabelLoaderError.png" />
                <br />
                <P>
                    Npm'den indirdiğimiz script kütüphanelerin versiyonları her zaman birbiriyle uyumlu olmayabiliyor ve resimde gördüğünüz gibi hata veriyor. Hatada; babel-core 6.x versiyonu babel-loader 8.x versiyonu ile uyumlu olmadığını söylüyor. <Pre>npm i babel-loader@7 –save-dev</Pre> yaparak hatayı düzeltebiliriz.
                </P>
                <br />
                <Divider />
                <br />
                <P>
                    Terminalde build komutunuzun başarılı bir şekilde çalıştığını gördüyseniz, şimdi de localhost'ta projeyi ayağa kaldıralım
                    <br></br>
                    Terminal'de <code>npm run-script start</code> komutunu çalıştıralım.
                </P>

                <ImageCard title="Sonuç" imgLink="MerhabaDunya.png" cardContent="Bu adımlardan sonra ekranda Merhaba Dünya ile karşılaştıysanız, doğru yoldasınız :)" />
            </div>
        )
    }
}
