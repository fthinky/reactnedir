import React, { Component } from 'react'
import H from '../components/h'
import P from '../components/p'
import ListItemLink from '../components/listItemLink'
import LinkIcon from '@material-ui/icons/Link';

export default class Introduction extends Component {
    render() {
        return (
            <div>
                <br />
                <div>
                    <H>Editör Seçimi</H>
                    <P>React geliştirmelerinizi text tabanlı editörlerle yapmanız size büyük kolaylık sağlayacaktır.</P>
                    <P>Text Editörlerler için aşağıdakileri inceleyebilirsiniz.</P>
                    <P>
                        <ListItemLink href="https://atom.io/" target="_blank" text="Atom">
                            <LinkIcon />
                        </ListItemLink>
                        <ListItemLink href="https://www.sublimetext.com/" target="_blank" text="Sublime">
                            <LinkIcon />
                        </ListItemLink>
                        <ListItemLink href="https://code.visualstudio.com/" target="_blank" text="Visual Studio Code">
                            <LinkIcon />
                        </ListItemLink>
                        Bu sitede örnek geliştirmelerimizi Visual Studio Code üzerinden anlatacağız.
                     </P>
                </div>
                <div>
                    <H>Node.js Nedir?</H>
                    <P>
                        Node.js açık kaynaklı bir sunucu ortamıdır. Node.js, sunucuda JavaScript çalıştırmanıza izin verir. Örnek çalışmalarımızı localhost ortamında çalıştırabilmek için node.js kurulumunu yapmanızı tavsiye ederiz.
                        <ListItemLink href="https://nodejs.org/en/" target="_blank" text="Node.js">
                            <LinkIcon />
                        </ListItemLink>
                    </P>
                </div>
                <div>
                    <H>JavaScript Versiyonları (ECMAScript)</H>
                    <P>JavaScript, 1995 yılında Brendan Eich tarafından icat edildi ve 1997'de ECMA standardı oldu.
                        JavaScript, ECMAScript standartlarında oluşturulmuş ve yaygın olarak web tarayıcılarında kullanılmakta olan dinamik bir programlama dilidir.
                        Bizde ES6 (ECMAScript 6) versiyonunda çalışmalarımızı yapacağız.
                    </P>
                </div>
                <div>
                    <H>Npm Nedir?</H>
                    <P>
                        NPM, Node.js paketleri için paket yöneticidir.
                        Npm, indirmek ve kullanmak için binlerce ücretsiz pakete ev sahipliği yapar.
                        <ListItemLink href="https://www.npmjs.com/" target="_blank" text="Npm">
                            <LinkIcon />
                        </ListItemLink>
                        Node.js'i yüklediğinizde NPM programı bilgisayarınıza yüklenir.
                        Bizde react çalışmalarımızıda, bazı paketlere ihtiyaç duyacağız. Kullanacağımız paketleri <code>npm install  [paket adı]</code> olarak indireceğiz. <code>npm i react</code>
                        Detaylı bilgiye bu linkten ulaşabilirsiniz.
                        <ListItemLink href="https://docs.npmjs.com/cli/install" target="_blank" text="Npm Döküman">
                            <LinkIcon />
                        </ListItemLink>
                        (Paket yöneticisine alternatif olarak YARN’da kullanabilirsiniz. YARN, Facebook desteğiyle geliştirilen npm alternatifi bir paket yöneticisidir.)
                    </P>
                </div>
            </div>
        )
    }
}
