import React, { Component } from 'react'
import Main from '../pages/main'
import FirstProject from '../pages/firstProject'
import Introduction from '../pages/introduction'
import DeveloperTools from '../pages/developerTools'
import ComponentsExample from '../pages/componentsExample'
import PropsExample from '../pages/propsExample'
import PropsChildren from '../pages/propsChildren'
import StateExample from '../pages/stateExample'
import CssExample from '../pages/cssExample'
import RouterExample from '../pages/routerExample'
import SetStateExample from '../pages/setStateExample'
import { Switch, Route } from 'react-router'


export default class Index extends Component {
    
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route path="/giris/" component={Introduction} />
                    <Route path="/ilk-proje/" component={FirstProject} />
                    <Route path="/react-developer-tools/" component={DeveloperTools} />
                    <Route path="/components/" component={ComponentsExample} />
                    <Route path="/props/" component={PropsExample} />
                    <Route path="/propsChildren/" component={PropsChildren} />
                    <Route path="/states/" component={StateExample} />
                    <Route path="/setState/" component={SetStateExample} />
                    <Route path="/css/" component={CssExample} />
                    <Route path="/router/" component={RouterExample} />
                </Switch>
            </div>
        )
    }
}
