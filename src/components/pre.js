import React, { Component } from 'react'
import PropTypes from 'prop-types'
export default class Pre extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <div className="gatsby-highlight" data-language="jsx">
                <pre className="language-jsx" spellcheck="false" contentEditable="true">
                    <span className="token">{this.props.children}</span>
                </pre>
            </div>
        )
    }
}

Pre.propTypes = {
};
Pre.defaultProps = {
};

