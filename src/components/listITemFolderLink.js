import React, { Component } from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FolderIcon from '@material-ui/icons/Folder';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import PropTypes from 'prop-types'
export default class listITemFolderLink extends Component {
    constructor(props){
        super(props);
        this.state = {
            open: false,
          };
    }

    handleClick = () => {
        this.setState(state => ({ open: !state.open }));
      };
    
  render() {
    debugger;
    return (
      <div>
        <ListItem button onClick={this.handleClick}>
                    <ListItemIcon>
                        <FolderIcon />
                    </ListItemIcon>
                    <ListItemText inset primary= {this.props.text} />
                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                       {this.props.children}
                    </List>
                </Collapse>
      </div>
    )
  }
}

listITemFolderLink.propTypes = {
  text: PropTypes.string.isRequired
  
};
listITemFolderLink.defaultProps = {
  text:""
};