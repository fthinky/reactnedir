import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types'
export default class P extends Component {
    constructor(props){
        super(props);
    }
  render() {
    return (
        <Typography gutterBottom paragraph variant={this.props.variant} style={{marginBottom: 0}}>
        {this.props.children}
      </Typography>
    )
  }
}

P.propTypes = {
    variant: PropTypes.string
  };
P.defaultProps = {
    variant: 'body1'
  };
  
