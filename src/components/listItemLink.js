import React, { Component } from 'react'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import LinkIcon from '@material-ui/icons/Link';
import PropTypes from 'prop-types'
function ListItemLnk(props) {
    return <ListItem button component="a" {...props} />;
  }

export default class ListItemLink extends Component {
    constructor(props){
        super(props);
        
    }
  render() {
    return (
        <ListItemLnk  href={this.props.href}>
        <ListItemIcon>
        {(this.props.children === undefined && !this.props.isFromFolder) &&
         <DashboardIcon />
      }
        {this.props.children}
        </ListItemIcon>
        <ListItemText primary={this.props.text} />
    </ListItemLnk>
    )
  }
}

ListItemLink.propTypes = {
    href: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    target:PropTypes.string,
    children: PropTypes.element,
    isFromFolder: PropTypes.bool
    
  };
  ListItemLink.defaultProps = {
    isFromFolder:false
};
