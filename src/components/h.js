import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types'
export default class H extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Typography variant={this.props.variant} gutterBottom>
                {this.props.children}
            </Typography>

        )
    }
}

H.propTypes = {
    variant: PropTypes.string
};
H.defaultProps = {
    variant: 'h6'
};

