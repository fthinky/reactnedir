import React, { Component } from 'react'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types'

const styles = {
    card: {
        maxWidth: 900,
    },
    media: {
        // ⚠️ object-fit is not supported by IE 11.
        objectFit: 'cover',
    },
};
class ImageCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { classes } = this.props;
        var imgLink = "../../style/images/" + this.props.imgLink;
        return (
            <Card className={classes.card}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        alt={this.props.title}
                        className={classes.media}
                        image={imgLink}
                        title={this.props.title}
                    />
                    {this.props.cardContent !== undefined &&
                        <CardContent>
                            {this.props.cardContent}
                        </CardContent>
                    }
                </CardActionArea>
                {this.props.children !== undefined &&
                    <CardActions>
                        {this.props.children}
                    </CardActions>
                }
            </Card>
        )
    }
}

ImageCard.propTypes = {
    title: PropTypes.string,
    imgLink: PropTypes.string,
    cardContent: PropTypes.string
};
ImageCard.defaultProps = {
};
export default withStyles(styles)(ImageCard);