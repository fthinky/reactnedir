import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import Home from '../src/pages/home'
import { HashRouter, Link } from "react-router-dom";

export default class App extends Component {
    render() {
        return (
            <div>
                <Home/>
            </div >
        )
    }
}
ReactDOM.render(<HashRouter><div><App /></div></HashRouter>, document.getElementById("root"));